create table if not exists `delivery_stores`
(
Year smallint unsigned not null,
Category varchar(30) not null,
IRI_KEY int unsigned not null,
OU varchar(7) not null,
EST_ACV decimal(10,9) not null,
Market_Name varchar(30) not null,
Open smallint unsigned not null,
Clsd smallint unsigned not null,
MskdName varchar(12) not null,
primary key (
	Year,
	Category,
	Open,
	Clsd
	)
)
;


create table if not exists `sales_by_store`
(
Year smallint unsigned not null,
Outlet char(1) not null,
Category varchar(30) not null,
IRI_KEY int unsigned not null,
WEEK smallint unsigned not null,
SY int unsigned not null,
GE int unsigned not null,
VEND int unsigned not null,
ITEM int unsigned not null,
UNITS smallint unsigned not null,
DOLLARS decimal(10,2) unsigned not null,
F varchar(4) not null,
D tinyint unsigned not null,
PR tinyint unsigned not null,
primary key (
	Year,
	Outlet,
	Category,
	IRI_KEY,
	WEEK,
	SY,
	GE,
	VEND,
	ITEM
	)
);

create table if not exists `prod_attr`
(
StartYear smallint unsigned not null,
EndYear smallint unsigned not null,
L1 tinyint unsigned not null,
L2 varchar(50) not null,
L3 varchar(50) not null,
L4 varchar(50) not null,
L5 varchar(50) not null,
L9 varchar(50) not null,
Level tinyint unsigned not null,
UPC varchar(20) not null,
SY int unsigned not null,
GE int unsigned not null,
VEND int unsigned not null,
ITEM int unsigned not null,
STUBSPEC varchar(100) not null,
VOL_EQ decimal(10,9) not null,
Attr1 varchar(50) not null,
Attr2 varchar(50) not null,
Attr3 varchar(50) not null,
Attr4 varchar(50) not null,
Attr5 varchar(50) not null,
Attr6 varchar(50) not null,
Attr7 varchar(50) not null,
primary key (
	StartYear,
	SY,
	GE,
	VEND,
	ITEM
	)
);


drop table if exists `label1`;
create table if not exists `label1`
(
Label tinyint unsigned not null,
Category varchar(50) not null,
primary key (
	Label
	)
);


insert into label1 VALUES( 0, 'UNDEF');
insert into label1 VALUES( 1, 'BEER/ALE/ALCOHOLIC CID');
insert into label1 VALUES( 2, 'BLADES');
insert into label1 VALUES( 3, 'CARBONATED BEVERAGES');
insert into label1 VALUES( 4, 'CIGARETTES');
insert into label1 VALUES( 5, 'COFFEE');
insert into label1 VALUES( 6, 'COLD CEREAL');
insert into label1 VALUES( 7, 'DEODORANT');
insert into label1 VALUES( 8, 'DIAPERS');
insert into label1 VALUES( 9, 'FACIAL TISSUE');
insert into label1 VALUES(10, 'FRANKFURTERS');
insert into label1 VALUES(11, 'FZ DINNERS/ENTREES');
insert into label1 VALUES(12, 'FZ PIZZA');
insert into label1 VALUES(13, 'HOUSEHOLD CLEANER');
insert into label1 VALUES(14, 'LAUNDRY DETERGENT');
insert into label1 VALUES(15, 'MARGARINE/SPREADS/BUTT');
insert into label1 VALUES(16, 'MAYONNAISE');
insert into label1 VALUES(17, 'MILK');
insert into label1 VALUES(18, 'MUSTARD & KETCHUP');
insert into label1 VALUES(19, 'PAPER TOWELS');
insert into label1 VALUES(20, 'PEANUT BUTTER');
insert into label1 VALUES(21, 'PHOTOGRAPHY SUPPLIES');
insert into label1 VALUES(22, 'RAZORS');
insert into label1 VALUES(23, 'SALTY SNACKS');
insert into label1 VALUES(24, 'SHAMPOO');
insert into label1 VALUES(25, 'SOUP');
insert into label1 VALUES(26, 'SPAGHETTI/ITALIAN SAUC');
insert into label1 VALUES(27, 'SUGAR SUBSTITUTES');
insert into label1 VALUES(28, 'TOILET TISSUE');
insert into label1 VALUES(29, 'TOOTHBRUSH/DENTAL ACCE');
insert into label1 VALUES(30, 'TOOTHPASTE');
insert into label1 VALUES(31, 'YOGURT');
































