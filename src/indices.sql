ALTER TABLE sales_by_store
ADD INDEX IX_SKU (
	SY ASC, 
	GE ASC, 
	VEND ASC, 
	ITEM ASC
)
;

ALTER TABLE `iris`.`prod_attr` 
ADD INDEX `IX_CO_BRAND` (`L3` ASC, `L4` ASC, `L5` ASC) 
, ADD INDEX `IX_StartYear_EndYear` (`StartYear` ASC, `EndYear` ASC) 
, ADD INDEX `IX_SKU` (`SY` ASC, `GE` ASC, `VEND` ASC, `ITEM` ASC) ;
