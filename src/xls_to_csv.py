import xlrd
import csv
import time


def get_category(category):
	if   category == 'CATEGORY - BEER/ALE/ALCOHOLIC CID':
		return 1
	elif category == 'CATEGORY - BLADES                ':
		return 2
	elif category == 'CATEGORY - CARBONATED BEVERAGES  ':
		return 3
	elif category == 'CATEGORY - CIGARETTES            ':
		return 4
	elif category == 'CATEGORY - COFFEE                ':
		return 5
	elif category == 'CATEGORY - COLD CEREAL           ':
		return 6
	elif category == 'CATEGORY - DEODORANT             ':
		return 7
	elif category == 'CATEGORY - DIAPERS               ':
		return 8
	elif category == 'CATEGORY - FACIAL TISSUE         ':
		return 9
	elif category == 'CATEGORY - FRANKFURTERS          ':
		return 10
	elif category == 'CATEGORY - FZ DINNERS/ENTREES    ':
		return 11
	elif category == 'CATEGORY - FZ PIZZA              ':
		return 12
	elif category == 'CATEGORY - HOUSEHOLD CLEANER     ':
		return 13
	elif category == 'CATEGORY - LAUNDRY DETERGENT     ':
		return 14
	elif category == 'CATEGORY - MARGARINE/SPREADS/BUTT':
		return 15
	elif category == 'CATEGORY - MAYONNAISE            ':
		return 16
	elif category == 'CATEGORY - MILK                  ':
		return 17
	elif category == 'CATEGORY - MUSTARD & KETCHUP     ':
		return 18
	elif category == 'CATEGORY - PAPER TOWELS          ':
		return 19
	elif category == 'CATEGORY - PEANUT BUTTER         ':
		return 20
	elif category == 'CATEGORY - PHOTOGRAPHY SUPPLIES  ':
		return 21
	elif category == 'CATEGORY - RAZORS                ':
		return 22
	elif category == 'CATEGORY - SALTY SNACKS          ':
		return 23
	elif category == 'CATEGORY - SHAMPOO               ':
		return 24
	elif category == 'CATEGORY - SOUP                  ':
		return 25
	elif category == 'CATEGORY - SPAGHETTI/ITALIAN SAUC':
		return 26
	elif category == 'CATEGORY - SUGAR SUBSTITUTES     ':
		return 27
	elif category == 'CATEGORY - TOILET TISSUE         ':
		return 28
	elif category == 'CATEGORY - TOOTHBRUSH/DENTAL ACCE':
		return 29
	elif category == 'CATEGORY - TOOTHPASTE            ':
		return 30
	elif category == 'CATEGORY - YOGURT                ':
		return 31
	else:
		return 0




def convert( from_xls, to_csv ):
    wb = xlrd.open_workbook(from_xls)
    sh = wb.sheet_by_index(0)
    csv_file = open(to_csv,'wb')
    wr = csv.writer(csv_file, quoting=csv.QUOTE_MINIMAL)
    for rownum in xrange(sh.nrows):
    	category = sh.row_values(rownum).pop(0)
    	if category == 'L1':
    	    # header row
    		wr.writerow(sh.row_values(rownum))
    	else:
            cat_value = get_category(category)
            newline = sh.row_values(rownum)
            newline.remove(category)
            newline.insert(0,unicode(str(cat_value)))
            wr.writerow(newline)
            #print newline
            #print "@@@@@"
            #time.sleep(10)
        
    csv_file.close()


