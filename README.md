# IRIs

Load the Marketing Science IRI data set into a MySQL database

## How it works

1. Obtain the IRI Marketing Data Set ([go here if you don't have it yet](http://pubsonline.informs.org/page/mksc/online-databases))
2. Download the code in this project via `git`
3. Create a build directory to store temporary files during construction of the database
4. Use CMake to configure the scripts
5. Issue `make all` from the root of the build directory in order to create the database

## Requirements

* [MySQL](http://www.mysql.com) - the database engine
* [Python](http://www.python.org) - to convert data from Excel® worksheet format to CSV
	* [xlrd](https://pypi.python.org/pypi/xlrd) - required by the Python scripts to extract data from Excel®
* [CMake](http://www.cmake.org) - to configure the scripts
* [Make](http://www.gnu.org/software/make/‎) - CMake can generate all sorts of things, but you will probably
want to create Makefiles. If you are working on Linux, you already have this. On Apple OS X®, get the version
supplied by Apple® with Xcode® (see [here](http://railsapps.github.io/xcode-command-line-tools.html)). On Windows®, 
I suggest the version included with [Cygwin](http://www.cygwin.com/).
